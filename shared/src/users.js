let _getStore;
let _fetch;

export function init(getStore, fetch) {
  _getStore = getStore;
  _fetch = fetch;
}

export function store() {
  return _getStore();
}

export function fetch(...args) {
  return _fetch(...args);
}
