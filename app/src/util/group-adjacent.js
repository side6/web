export default function groupAdjacentBy({ predicate, initGroup, combine }) {
  return array => {
    if (array.length === 0) {
      return [];
    }

    let [head, ...tail] = array;

    let result = [];
    let currentGroup = initGroup(head);
    let previousElement = head;

    for (const element of tail) {
      if (predicate(currentGroup, previousElement, element)) {
        currentGroup = combine(currentGroup, element);
      } else {
        result.push(currentGroup);
        currentGroup = initGroup(element);
      }
      previousElement = element;
    }
    result.push(currentGroup);

    return result;
  };
}
