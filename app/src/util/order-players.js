export default function orderPlayers(allPlayers, playerId) {
  let playerIndex = allPlayers.findIndex(player => player.id === playerId);

  if (playerIndex === -1) {
    return allPlayers;
  } else {
    let player = allPlayers[playerIndex];

    let playersBeforePlayer = allPlayers.slice(0, playerIndex);
    let playersAfterPlayer = allPlayers.slice(playerIndex + 1);

    return [player, ...playersAfterPlayer, ...playersBeforePlayer];
  }
}
