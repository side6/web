import * as sapper from "@sapper/app";
import { initClientStores } from "./stores";

initClientStores().then(() => {
  sapper.start({
    target: document.querySelector("#sapper")
  });
});
