export function isActing(phase, playerId) {
  if (phase.draw) {
    return phase.draw.playerId === playerId;
  } else if (phase.choose) {
    return phase.choose.playerId === playerId;
  }
}
