export function maxPlayers(gameType, settings) {
  switch (gameType) {
    case "love-letter":
      return Math.max(...settings.selectedPlayerCounts);
    case "fox-in-the-forest":
      return 2;
    case "no-thanks":
      return Math.max(...settings.selectedPlayerCounts);
    case "shobu":
      return 2;
    case "skull":
      return Math.max(...settings);
    case "the-resistance": {
      return Math.max(...settings.selectedPlayerCounts);
    }
    case "tichu":
      return 4;
  }
}
