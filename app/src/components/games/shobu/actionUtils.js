export function isActing(phase, playerColor) {
  if (phase.passive) {
    return playerColor === phase.passive;
  } else if (phase.aggressive) {
    return playerColor === phase.aggressive.player;
  }
}
