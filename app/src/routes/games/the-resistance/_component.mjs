let componentPromise = Promise.resolve();

if (process.browser) {
  componentPromise = import("@gamesite6/the-resistance").then((components) => {
    const { Game, Reference } = components.default;
    window.customElements.define("gs6-games-the-resistance", Game);
    window.customElements.define(
      "gs6-games-the-resistance-reference",
      Reference
    );
  });
}

export default componentPromise;
