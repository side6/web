let componentPromise = Promise.resolve();

if (process.browser) {
  componentPromise = import("@gamesite6/tichu").then((components) => {
    const { Game, Reference } = components.default;
    window.customElements.define("gs6-games-tichu", Game);
    window.customElements.define("gs6-games-tichu-reference", Reference);
  });
}

export default componentPromise;
