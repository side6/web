let componentPromise = Promise.resolve();

if (process.browser) {
  componentPromise = import("@gamesite6/skull").then((GameComponent) => {
    window.customElements.define("gs6-games-skull", GameComponent);
  });
}

export default componentPromise;
