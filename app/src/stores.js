import { writable } from "svelte/store";
import * as shared from "@gamesite6/shared";

shared.users.init(getUsersStore, getUserData);

let websocket;

// stores
let user;
let users;
const chatrooms = {};
const games = {};
const lobbies = {};

function websocketListener(event) {
  const msg = JSON.parse(event.data);

  if (msg.auth) {
    user.set({ id: msg.auth, games: null });
  } else if (msg.lobby) {
    const lobby = getLobbyStore(msg.lobby.game_type);
    lobby.set(msg.lobby);
  } else if (msg.games) {
    user.update((state) => {
      const newGames = {};
      for (const game of msg.games.games) {
        newGames[game.data.id] = game;
      }

      state.games = newGames;
      return state;
    });
  } else if (msg.created) {
    const lobby = getLobbyStore(msg.created.game.data.game_type);
    lobby.update((state) => {
      state.games.unshift(msg.created.game);
      return state;
    });
    const game = msg.created.game;
    const gameId = game.data.id;
    const gameHost = game.data.host;
    user.update((state) => {
      if (
        state &&
        (state.id === gameHost || game.data.players.includes(state.id))
      ) {
        state.games = state.games || {};
        state.games[gameData.id] = game;
      }
      return state;
    });
  } else if (msg.chat) {
    const room = msg.chat.room;
    const store = getChatroomStore(room);

    const chatMessage = {
      userId: msg.chat.usr,
      text: msg.chat.msg,
      time: Date.parse(msg.chat.time),
    };
    store.update((messages) => {
      messages.push(chatMessage);
      return messages;
    });
  } else if (msg.chats) {
    const room = msg.chats.room;
    const store = getChatroomStore(room);
    const messages = msg.chats.msgs.map((msg) => {
      return {
        userId: msg.usr,
        text: msg.msg,
        time: Date.parse(msg.time),
      };
    });
    store.set(messages);
  } else if (msg.updated) {
    const gameId = msg.updated.game.id;
    const store = getGameStore(gameId, msg.updated.game.game_type);
    store.update((prev) => {
      let game = prev || {};
      game.data = msg.updated.game;
      return game;
    });

    if (msg.updated.game.status === "NOT_STARTED") {
      const lobby = getLobbyStore(msg.updated.game.game_type);
      lobby.update((state) => {
        let existingGame = state.games.find((g) => g.data.id === gameId);
        if (existingGame) {
          existingGame.data = msg.updated.game;
        }
        return state;
      });
    }

    user.update((state) => {
      if (state && state.id) {
        const userId = state.id;
        if (
          userId === msg.updated.game.host ||
          msg.updated.game.players.includes(userId)
        ) {
          state.games = state.games || {};
          state.games[gameId] = state.games[gameId] || {};
          state.games[gameId].data = msg.updated.game;
        } else if (state.games) {
          delete state.games[gameId];
        }
      }
      return state;
    });
  } else if (msg.game) {
    const gameId = msg.game.id;
    const store = getGameStore(gameId);
    store.update((prev) => {
      const game = prev || {};
      if (msg.game.data) {
        game.data = msg.game.data;
      }
      if (msg.game.settings) {
        game.settings = msg.game.settings;
      }
      if (msg.game.state) {
        game.state = msg.game.state;
      }
      return game;
    });

    if (msg.game.data && msg.game.data.status === "NOT_STARTED") {
      const lobby = getLobbyStore(msg.game.data.game_type);
      lobby.update((state) => {
        const existingGame = state.games.find(
          (g) => g.data.id == msg.game.data.id
        );
        if (existingGame) {
          existingGame.data = msg.game.data;
          if (msg.game.settings) {
            existingGame.settings = msg.game.settings;
          }
        }
        return state;
      });
    }

    user.update((state) => {
      if (state && state.id && msg.game.data) {
        const userId = state.id;
        if (
          userId === msg.game.data.host ||
          msg.game.data.players.includes(userId)
        ) {
          state.games = state.games || {};
          const existingGame = state.games[msg.game.data.id];

          state.games[msg.game.data.id] = msg.game;

          if (!msg.game.settings && existingGame) {
            state.games[msg.game.data.id].settings = existingGame.settings;
          }
          if (!msg.game.state && existingGame) {
            state.games[msg.game.data.id].state = existingGame.state;
          }
        } else if (state.games) {
          delete state.games[msg.game.data.id];
        }
      }
      return state;
    });
  } else if (msg.started) {
    const gameId = msg.started.id;

    const lobby = getLobbyStore(msg.started.game_type);
    lobby.update((state) => {
      let gameIndex = state.games.findIndex((g) => g.data.id === gameId);
      if (gameIndex !== -1) {
        state.games.splice(gameIndex, 1);
      }
      return state;
    });
  }
}

export function initServerStores() {
  websocket = {
    send() {},
    addEventListener() {},
    removeEventListener() {},
  };
  user = writable(null);
  users = writable({});
}

export async function initClientStores() {
  await new Promise((resolve) => {
    websocket = new WebSocket(`${API_TLS ? "wss" : "ws"}://${API_HOST}/api/ws`);

    function handleOpen() {
      websocket.send(JSON.stringify({ auth: null }));
      websocket.removeEventListener("open", handleOpen);
      resolve();
    }
    websocket.addEventListener("open", handleOpen);
    websocket.addEventListener("message", websocketListener);
  });

  user = writable(null);
  users = writable({});
}

export function joinGame(gameId, gameType) {
  websocket.send(JSON.stringify({ join: { id: gameId, game_type: gameType } }));
}

export function leaveGame(gameId, gameType) {
  websocket.send(
    JSON.stringify({ leave: { id: gameId, game_type: gameType } })
  );
}

export function startGame(gameId, gameType) {
  websocket.send(
    JSON.stringify({ start: { id: gameId, game_type: gameType } })
  );
}

export function performAction(gameId, gameType, action) {
  websocket.send(
    JSON.stringify({
      act: {
        game_id: gameId,
        game_type: gameType,
        action,
      },
    })
  );
}

export function getLobbyStore(gameType) {
  if (!lobbies[gameType]) {
    const store = writable({ games: [] }, function start(set) {
      set({ games: [] });
      websocket.send(JSON.stringify({ "lobby+": gameType }));

      return function stop() {
        websocket.send(JSON.stringify({ "lobby-": gameType }));
      };
    });
    lobbies[gameType] = store;
  }

  return lobbies[gameType];
}

export function getChatroomStore(room) {
  if (!chatrooms[room]) {
    const store = writable([], function start(set) {
      set([]);
      websocket.send(JSON.stringify({ "chat+": room }));

      return function stop() {
        websocket.send(JSON.stringify({ "chat-": room }));
        delete chatrooms[room];
      };
    });

    chatrooms[room] = store;
  }
  return chatrooms[room];
}

export function getGameStore(gameId, gameType) {
  if (games[gameId]) {
    return games[gameId];
  }

  if (!gameId || !gameType) {
    return writable(null);
  }

  if (!games[gameId]) {
    const store = writable(null, function start(set) {
      set(null);
      websocket.send(
        JSON.stringify({ "game+": { id: gameId, game_type: gameType } })
      );

      return function stop() {
        websocket.send(JSON.stringify({ "game-": gameId }));
        delete games[gameId];
      };
    });
    games[gameId] = store;
  }

  return games[gameId];
}

export async function createGame(gameType, settings) {
  const res = await fetch(
    `${API_TLS ? "https" : "http"}://${API_HOST}/api/games`,
    {
      method: "POST",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        game_type: gameType,
        settings,
      }),
    }
  );
  if (res.ok) {
    const json = await res.json();
    return json;
  }
}

export function getUserStore() {
  return user;
}

export function sendMessage({ room, msg }) {
  websocket.send(
    JSON.stringify({
      chat: {
        room,
        msg,
      },
    })
  );
}

export function getUsersStore() {
  return users;
}

const userDataRequests = {};
export async function getUserData(userId) {
  if (userDataRequests[userId]) {
    return await userDataRequests[userId];
  } else {
    const req = fetch(
      `${API_TLS ? "https" : "http"}://${API_HOST}/api/users/${userId}`
    ).then((res) => res.json());
    userDataRequests[userId] = req;
    const userData = await req;

    users.update((state) => {
      state[userId] = userData;
      return state;
    });

    return userData;
  }
}
