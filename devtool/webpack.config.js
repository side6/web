require("dotenv").config();
const webpack = require("webpack");

const mode = "development";
const dev = mode === "development";

module.exports = {
  entry: ["./src/index.js"],
  mode,
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/elm-stuff/, /node_modules/],
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
      },
      {
        test: /\.elm$/,
        exclude: [/elm-stuff/, /node_modules/],
        use: {
          loader: "elm-webpack-loader",
          options: {
            debug: dev,
          },
        },
      },
    ],
  },
  resolve: {
    alias: {
      "@gamesite6/game": process.env.GAME_CLIENT_PATH,
    },
  },
  devtool: dev ? "inline-source-map" : "source-map",
};
