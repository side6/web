port module Main exposing (main)

import Browser
import Html exposing (Html, aside, button, div, fieldset, input, label, legend, node, p, text, textarea)
import Html.Attributes as Attr exposing (attribute, checked, class, disabled, for, id, max, min, name, property, type_, value)
import Html.Events exposing (on, onClick, onInput)
import Http
import Json
import Json.Decode as D
import Json.Encode as E


main : Program D.Value Model Msg
main =
    Browser.document
        { init = init
        , subscriptions = subscriptions
        , update = update
        , view = view
        }


type alias PlayerId =
    Int


type alias Model =
    { playerCount : Int
    , userId : Maybe PlayerId
    , settings : String
    , settingsInput : String
    , state : String
    , stateInput : String
    }


jsonEncModel : Model -> E.Value
jsonEncModel model =
    E.object
        [ ( "playerCount", E.int model.playerCount )
        , ( "userId", Maybe.withDefault E.null (Maybe.map E.int model.userId) )
        , ( "settings", E.string model.settings )
        , ( "settingsInput", E.string model.settingsInput )
        , ( "state", E.string model.state )
        , ( "stateInput", E.string model.stateInput )
        ]


jsonDecModel : D.Decoder Model
jsonDecModel =
    D.map6
        Model
        (D.field "playerCount" D.int)
        (D.maybe (D.field "userId" D.int))
        (D.field "settings" D.string)
        (D.field "settingsInput" D.string)
        (D.field "state" D.string)
        (D.field "stateInput" D.string)


init : D.Value -> ( Model, Cmd Msg )
init flags =
    case D.decodeValue jsonDecModel flags of
        Ok model ->
            ( model, Cmd.none )

        Err _ ->
            ( { playerCount = 4
              , userId = Nothing
              , settings = ""
              , settingsInput = ""
              , state = ""
              , stateInput = ""
              }
            , Cmd.none
            )


port modelJsonChanged : E.Value -> Cmd msg


modelChanged : Model -> Cmd msg
modelChanged model =
    modelJsonChanged (jsonEncModel model)


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


type Input
    = SettingsInput
    | StateInput


type Msg
    = RequestInitialState
    | InitialStateReceived (Result Http.Error E.Value)
    | SetUserId (Maybe PlayerId)
    | ChangePlayerCount String
    | Apply Input
    | Change Input String
    | Reset Input
    | PerformAction E.Value
    | ActionResponseReceived (Result Http.Error Json.PerformActionRes)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        RequestInitialState ->
            case D.decodeString D.value model.settings of
                Ok settingsValue ->
                    ( model
                    , Http.post
                        { url = "/initial-state"
                        , body =
                            Http.jsonBody
                                (Json.jsonEncInitialStateReq
                                    { players = List.range 1 model.playerCount
                                    , settings = settingsValue
                                    , seed = 42
                                    }
                                )
                        , expect = Http.expectJson InitialStateReceived D.value
                        }
                    )

                Err _ ->
                    ( model, Cmd.none )

        InitialStateReceived (Ok initialStateRes) ->
            case D.decodeValue Json.jsonDecInitialStateRes initialStateRes of
                Ok { state } ->
                    let
                        nextState =
                            E.encode 2 state

                        nextModel =
                            { model | state = nextState, stateInput = nextState }
                    in
                    ( nextModel, modelChanged nextModel )

                Err err ->
                    Debug.log
                        (Debug.toString err)
                        ( model, Cmd.none )

        InitialStateReceived (Err _) ->
            ( model, Cmd.none )

        SetUserId userId ->
            let
                nextModel =
                    { model | userId = userId }
            in
            ( nextModel, modelChanged nextModel )

        ChangePlayerCount value ->
            case String.toInt value of
                Just playerCount ->
                    ( { model | playerCount = playerCount }, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )

        Change SettingsInput value ->
            let
                nextModel =
                    { model | settingsInput = value }
            in
            ( nextModel, modelChanged nextModel )

        Change StateInput value ->
            let
                nextModel =
                    { model | stateInput = value }
            in
            ( nextModel, modelChanged nextModel )

        Apply SettingsInput ->
            let
                nextModel =
                    { model | settings = model.settingsInput }
            in
            ( nextModel, modelChanged nextModel )

        Apply StateInput ->
            let
                nextModel =
                    { model | state = model.stateInput }
            in
            ( nextModel, modelChanged nextModel )

        Reset SettingsInput ->
            let
                nextModel =
                    { model | settingsInput = model.settings }
            in
            ( nextModel, modelChanged nextModel )

        Reset StateInput ->
            let
                nextModel =
                    { model | stateInput = model.state }
            in
            ( nextModel, modelChanged nextModel )

        PerformAction actionJson ->
            case ( model.userId, D.decodeString D.value model.state, D.decodeString D.value model.settings ) of
                ( Just userId, Ok stateJson, Ok settingsJson ) ->
                    ( model
                    , Http.post
                        { url = "/perform-action"
                        , body =
                            Http.jsonBody
                                (Json.jsonEncPerformActionReq
                                    { performedBy = userId
                                    , action = actionJson
                                    , state = stateJson
                                    , settings = settingsJson
                                    , seed = 42
                                    }
                                )
                        , expect = Http.expectJson ActionResponseReceived Json.jsonDecPerformActionRes
                        }
                    )

                ( _, _, _ ) ->
                    ( model, Cmd.none )

        ActionResponseReceived (Ok res) ->
            let
                nextState =
                    E.encode 2 res.nextState

                nextModel =
                    { model | state = nextState, stateInput = nextState }
            in
            ( nextModel, modelChanged nextModel )

        ActionResponseReceived (Err err) ->
            Debug.log
                (Debug.toString err)
                ( model, Cmd.none )


view : Model -> Browser.Document Msg
view model =
    { title = "Devtool - Gamesite 6"
    , body =
        [ div
            [ class "devtool-game" ]
            [ gameView model ]
        , aside
            [ class "devtool-aside" ]
            [ div
                [ class "reference" ]
                [ referenceView model ]
            , devtoolControls model
            ]
        ]
    }


devtoolControls : Model -> Html Msg
devtoolControls model =
    div []
        [ fieldset []
            [ legend [] [ label [ for "player-count" ] [ text "player count" ] ]
            , input
                [ id "player-count"
                , type_ "number"
                , Attr.min "1"
                , Attr.max "10"
                , onInput ChangePlayerCount
                , value (String.fromInt model.playerCount)
                ]
                []
            , div []
                [ button
                    [ Attr.style "margin-top" "0.2em"
                    , class "primary small"
                    , id "new-game"
                    , onClick RequestInitialState
                    ]
                    [ text "new game" ]
                ]
            ]
        , fieldset []
            [ legend [] [ text "user id" ]
            , div
                []
                (List.concat
                    [ [ input
                            [ onClick (SetUserId Nothing)
                            , checked (model.userId == Nothing)
                            , type_ "radio"
                            , id "user-none"
                            , name "user"
                            , value ""
                            ]
                            []
                      , label [ for "user-none" ] [ text "none " ]
                      ]
                    , List.range 1 model.playerCount
                        |> List.concatMap
                            (\playerId ->
                                let
                                    inputId =
                                        "user-" ++ String.fromInt playerId
                                in
                                [ input
                                    [ onClick (SetUserId (Just playerId))
                                    , checked (model.userId == Just playerId)
                                    , type_ "radio"
                                    , id inputId
                                    , name "user"
                                    , value (String.fromInt playerId)
                                    ]
                                    []
                                , label [ for inputId ] [ text (String.fromInt playerId ++ " ") ]
                                ]
                            )
                    ]
                )
            ]
        , fieldset []
            [ legend [] [ label [ for "settings-input" ] [ text "settings" ] ]
            , textarea [ id "settings-input", value model.settingsInput, onInput (Change SettingsInput) ] []
            , div []
                [ button
                    [ class "secondary small"
                    , id "reset-settings"
                    , disabled (model.settings == model.settingsInput)
                    , onClick (Reset SettingsInput)
                    ]
                    [ text "reset" ]
                , text " "
                , button
                    [ class "primary small"
                    , id "apply-settings"
                    , disabled (model.settings == model.settingsInput)
                    , onClick (Apply SettingsInput)
                    ]
                    [ text "apply" ]
                ]
            ]
        , fieldset []
            [ legend [] [ label [ for "state-input" ] [ text "state" ] ]
            , textarea [ id "state-input", value model.stateInput, onInput (Change StateInput) ] []
            , div []
                [ button
                    [ class "secondary small"
                    , id "reset-state"
                    , disabled (model.state == model.stateInput)
                    , onClick (Reset StateInput)
                    ]
                    [ text "reset" ]
                , text " "
                , button
                    [ class "primary small"
                    , id "apply-state"
                    , disabled (model.state == model.stateInput)
                    , onClick (Apply StateInput)
                    ]
                    [ text "apply" ]
                ]
            ]
        ]


gameView : Model -> Html.Html Msg
gameView model =
    case ( D.decodeString D.value model.settings, D.decodeString D.value model.state ) of
        ( Err err1, Err err2 ) ->
            div []
                [ p [] [ text (D.errorToString err1) ]
                , p [] [ text (D.errorToString err2) ]
                ]

        ( Err err, _ ) ->
            text (D.errorToString err)

        ( _, Err err ) ->
            text (D.errorToString err)

        ( Ok settings, Ok state ) ->
            node "game-component"
                [ on "action" (D.map PerformAction (D.field "detail" D.value))
                , property "state" state
                , property "settings" settings
                , attribute "user-id" (Maybe.withDefault "" (Maybe.map String.fromInt model.userId))
                ]
                []


referenceView : Model -> Html.Html Msg
referenceView model =
    case D.decodeString D.value model.settings of
        Ok settings ->
            node "reference-component"
                [ property "settings" settings ]
                []

        Err err ->
            text (D.errorToString err)
